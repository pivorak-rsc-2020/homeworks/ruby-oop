class Catalog
  attr_reader :items_list

  def initialize
    # your code goes here
  end

  def add(item)
    # your code goes here
  end

  def remove(code)
    # your code goes here
  end

  def show(code)
    # your code goes here
  end

  def list
    # your code goes here
  end

  private

  def find_item_by(code)
    # your code goes here
  end
end
